package main;

import java.util.Random;

public class Sorteador {
	private static Random randomValores = new Random();
	private static Random randomNaipes = new Random();
	
	public static int sortearValor (int quantidade) {
		return randomValores.nextInt(quantidade);
	}
	
	public static int sortearNaipe (int quantidade) {
		return randomNaipes.nextInt(quantidade);
	}
}
