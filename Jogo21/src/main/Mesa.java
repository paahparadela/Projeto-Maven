package main;

public class Mesa {
	private static int sorteValor, sorteNaipe, tamanhoVetorValor, tamanhoVetorNaipe;
	
	public static String sortearValorCarta() {
		tamanhoVetorValor = Cartas.vetorValores().length;
		sorteValor = Sorteador.sortearValor(tamanhoVetorValor);
		return Cartas.valores(sorteValor);
	}
	
	public static String sortearNaipeCarta() {
		tamanhoVetorNaipe = Cartas.vetorNaipes().length;
		sorteNaipe = Sorteador.sortearNaipe(tamanhoVetorNaipe);
		return Cartas.naipes(sorteNaipe);
	}
}
