package main;

public class Cartas {
	private static String [] valores = {"As", "2", "3", "4", "5", "6", "7", "8", "9", "10", "Valete", "Dama", "Rei"};
	private static String [] naipes = {"Paus", "Ouros", "Copas", "Espadas"};
	
	public static String valores(int index){
		return valores[index];
	}
	
	public static String [] vetorValores(){
		return valores;
	}
	
	public static String naipes(int index){
		return naipes[index];
	}
	
	public static String [] vetorNaipes(){
		return naipes;
	}
}
