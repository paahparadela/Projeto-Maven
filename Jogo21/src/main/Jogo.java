package main;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Jogo {

	public static void Menu() {
		System.out.println("***************** Jogo 21 *****************");
		System.out.println("Escolha as opções: ");
		System.out.println("1. Jogar modo single-player ");
		System.out.println("0. Sair ");
	}

	public static int validarCarta(String valorCarta) {
		int valorCartaConvertido;
		if (!(valorCarta.equals("As")) && !(valorCarta.equals("Valete")) && !(valorCarta.equals("Dama"))
				&& !(valorCarta.equals("Rei"))) {
			valorCartaConvertido = Integer.parseInt(valorCarta);
		} else {
			if (valorCarta.equals("As")) {
				valorCartaConvertido = 1;
			} else {
				valorCartaConvertido = 10;
			}
		}
		return valorCartaConvertido;
	}

	public static void validarPontos(int totalPontos) {
		if (totalPontos == 21) {
			System.out.println();
			System.out.println("Você ganhou!");
		} else {
			System.out.println();
			System.out.println("Você perdeu :/");
		}
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String menu = "";
		Scanner ler = new Scanner(System.in);
		String carta;
		String valorCarta = "";
		String naipeCarta = "";
		int valorCartaConvertido;
		int totalPontos;
		List<Integer> listaCartas = new ArrayList();

		Menu();
		menu = ler.nextLine();

		while (!menu.equals("0")) {
			System.out.println();
			System.out.println("Pressione enter para pegar uma carta do baralho!");
			menu = ler.nextLine();
			
			valorCarta = Mesa.sortearValorCarta();
			naipeCarta = Mesa.sortearNaipeCarta();

			carta = valorCarta + " de " + naipeCarta;
			
			System.out.println("Sua carta sorteada é: " + carta);

			valorCartaConvertido = validarCarta(valorCarta);

			listaCartas.add(valorCartaConvertido);

			totalPontos = Calculador.calcularPontos(listaCartas);
			
			System.out.println("Pontuação: "+totalPontos);

			if (totalPontos >= 21) {
				validarPontos(totalPontos);
				return;
			}

		}
		System.out.println("Fim do jogo");

	}

}
