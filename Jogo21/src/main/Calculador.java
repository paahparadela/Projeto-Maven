package main;

import java.util.List;

public class Calculador {
	public static int calcularPontos(List<Integer> cartas) {
		int totalPontos = 0;
		for(int i = 0; i < cartas.size(); i++) {
			totalPontos += cartas.get(i);
		}
		return totalPontos;
	}
}
